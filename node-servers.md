## Resources

### Code

- http
- [express](http://expressjs.com/)

- [hapi](https://hapijs.com)
- [total](https://www.totaljs.com/)
- [fastify](https://www.fastify.io/)

### Manager (supervisor)

- pm2
- forever
- nodemon
- supervisor

### Sources

- https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework

